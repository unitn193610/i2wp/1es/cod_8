# HOMEWORK 2

SECURITY

Our implementation turns file location into relative paths, thus allowing the client to access potentially any file in the local filesystem. Modify our implementation to limit the access to files and directories under the current folder (of the server, analogous to the “document root” or “www” folders in http servers).